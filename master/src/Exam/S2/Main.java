package Exam.S2;

public class Main {
    public static void main(String[] Args){
        //Creating the threads
        MyThread t1 = new MyThread("MyThread1");
        MyThread t2 = new MyThread("MyThread2");
        MyThread t3 = new MyThread("MyThread3");

        //Starting the threads
        t1.start();
        t2.start();
        t3.start();
    }
}

class MyThread extends Thread{
    private static int count=0; //Counter to hold the number of written messages
    private static final Object lock=new Object(); //Lock for synchronisation

    public MyThread(String name){
        super(name);
    }

    public void run(){
        //All threads will run, until the count reaches 9
        while (count<10){
                synchronized (lock){
                    //The while condition was not synchronised, so we have to check the condition again
                    if (count<10){
                        System.out.println(this.getName()+" "+count);
                        count++;
                        try {
                            Thread.sleep((int)(1000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
            }

        }

    }


}
