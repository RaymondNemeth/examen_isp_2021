package Exam.S1;

public class M {
    public A a;
    private static B b;

    public M(A a){
        this.a=a;
        b=new B();
    }
}

interface I{
    public void i();
}

class L implements I{
    private int a;
    private M m;

    public void b(){}

    @Override
    public void i() {

    }
}

class X{
    public void met(L l){}
}

class A{}

class B{}

class Main{
    public static void main(String[] Args){
        A a =new A();
        M m =new M(a);
        X x= new X();
        L l= new L();
    }
}